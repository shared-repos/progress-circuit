import axios from 'axios';
import store from './store/store';

const instance = axios.create();

instance.interceptors.request.use( async config => {
  config.baseURL = await store.getters.hostEndpoint
  return config
},
error => Promise.reject(error))

export default instance;