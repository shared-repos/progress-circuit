import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import { routes } from './routes';
import store from './store/store';

Vue.use(VueRouter);

Vue.config.productionTip = false;

const router = new VueRouter({
  mode: process.env.IS_ELECTRON ? 'hash' : 'history',
  routes
});

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app');
