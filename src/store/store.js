import Vue from 'vue';
import Vuex from 'vuex';

import themes from './modules/themes';
import circuits from './modules/circuits';
import ic from './modules/ic';
import log from './modules/log';
import host from './modules/host'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    themes,
    circuits,
    ic,
    log,
    host
  }
});