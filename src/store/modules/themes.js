import axios from '../../axios'

const state = {
  themes: [],
  activeThemeId: 0,
  isLoading: false
};

const mutations = {
  'INIT_THEMES' (state) {
    state.isLoading = true;
    axios.get('/themes')
    .then(response => response.data)
    .then(result => {
      state.themes = result
    })
    .catch(error => {
      // eslint-disable-next-line
      console.log(`Something went wrong while retriving themes: ${error}`)
    })
    .finally(() => {
      state.isLoading = false;
    })
  },
  'SET_ACTIVE_THEME' (state, id) {
    state.activeThemeId = id;
  }
};

const actions = {
  initThemes({ commit }) {
    commit('INIT_THEMES');
  },
  setActiveTheme({ commit }, id) {
    commit('SET_ACTIVE_THEME', id);
  }
};

const getters = {
  themes (state) {
    return state.themes;
  },
  isThemesLoading(state) {
    return state.isLoading
  },
  activeTheme(state) {
    return state.activeThemeId
  }
};

export default {
  state,
  mutations,
  actions,
  getters
}