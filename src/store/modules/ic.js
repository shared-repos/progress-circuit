import axios from '../../axios'
import Vue from 'vue'

const state = {
  circuit: [],
  pars: [],
  isLoading: false
};

const mutations = {
  'LOAD_CIRCUIT' (state, { icId }) {
    state.isLoading = true;
    axios.get(`/circuits/${icId}`)
    .then(response => response.data)
    .then(result => {
      state.circuit = result
    })
    .then(state.pars = [])
    .then(function() {
      state = fillParams(state)
    })
    .catch(error => {
      // eslint-disable-next-line
      console.log(`Something went wrong while retriving circuit: ${error}`)
    })
    .finally(() => {
      state.isLoading = false;
    })
  },
  'CHANGE_VALUE' (state, { icId, setting }) {
    state.isLoading = true;
    axios.patch(`/circuits/${icId}`, getPatchBody(setting))
    .then(response => response.data)
    .then(result => {
      state.circuit = result
    })
    .then(state.pars = [])
    .then(function() {
      state = fillParams(state)
    })
    .catch(error => {
      // eslint-disable-next-line
      console.log(`Something went wrong while retriving circuit: ${error}`)
    })
    .finally(() => {
      state.isLoading = false;
    })
  }
};

const actions = {
  loadCircuit({ commit, rootState }) {
    commit('LOAD_CIRCUIT', {
      themeId: rootState.themes.activeThemeId,
      icId: rootState.circuits.activeCircuitId
    })
  },
  changeValue({ commit, rootState }, setting) {
    commit('CHANGE_VALUE', {
      icId: rootState.circuits.activeCircuitId,
      setting
    })
  },
  sendRegister({ dispatch }, { reg, writeToLog = true }) {
    state.isLoading = true;
    axios.post(`/regs`, {
      regList: reg.name
    })
    .then(response => response.data)
    .then(result => {
      if (writeToLog) {
        dispatch('addLogRecord', `Send ${reg.name}, response is "${JSON.stringify(result[0])}"`)
      }
    })
    .catch(error => {
      // eslint-disable-next-line
      console.log(`Something went wrong while retriving circuit: ${error}`)
    })
    .finally(() => {
      state.isLoading = false;
    })
  },
  resetSettings({ dispatch }) {
    dispatch('loadCircuit')
    dispatch('addLogRecord', `Reset all circuit settings`)
    state.circuit.regs.map(x => {
      dispatch('sendRegister', { reg: x, writeToLog: false })
    })
  }
};

const getters = {
  circuit (state) {
    return state.circuit
  },
  circuitBlocks (state) {
    return state.circuit.blocks
  },
  isCircuitLoading(state, rootState) {
    return rootState.activeCircuit == 0 || state.isLoading
  },
  settingParameters: (state) => (blockName) => {
    return state.pars.filter(x => {
      return x.part == blockName
    })
  },
  registers (state) {
    return state.circuit.regs
  }
};

function fillParams(state) {
  for (let reg of state.circuit.regs) {
    Object.keys(reg.data).map(x => {
      Vue.set(reg.data[x], 'value', reg.data[x].value ? reg.data[x].value : reg.data[x].bits[2])
      Vue.set(reg.data[x], 'name', reg.data[x].name ? reg.data[x].name : reg.name)
      Vue.set(reg.data[x], 'id', x)
      state.pars.push(reg.data[x])
    })
  }

  state.circuit.pars.map(x => {
    state.pars.push(x)
  })

  return state
}

function getPatchBody(setting) {
  if (setting.name.indexOf('reg') != -1 && setting.name.indexOf('x') != -1) {
    return {
      regs: {
        name: setting.name,
        value: setting.value,
        parName: setting.id
      }
    }
  } else {
    return {
      pars: {
        name: setting.name,
        value: setting.value
      }
    }
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}