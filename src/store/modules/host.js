import axios from "../../axios";

const state = {
  ip: '',
  isLoading: false,
  loaded: false
};

const mutations = {
  'SET_IP' (state, host) {
    state.ip = host
  }
};

const actions = {
  connect ({ commit, dispatch }, host) {
    state.isLoading = true;
    axios.get(`${host}/healthcheck`)
    .then(response => {
      if (response.status == 200) {
        commit('SET_IP', host)
        state.loaded = true
        localStorage.setItem('host', host)
        dispatch('initThemes')
        dispatch('loadAllCircuits')
      }
    })
    .catch(error => {
      //eslint-disable-next-line
      console.log(`Something went wrong while retrieving endpoint. Error: ${error}`)
    })
    .finally(() => {
      state.isLoading = false
    })
  },
  initHost ({ commit }) {
    let existingHost = localStorage.getItem('host');

    if (existingHost) {
      commit('SET_IP', existingHost)
    }
  },
  setHost ({ commit }, host) {
    commit('SET_IP', host)
  }
};

const getters = {
  hostEndpoint(state) {
    return state.ip
  },
  hostLoaded(state) {
    return state.loaded
  },
  hostLoading(state) {
    return state.isLoading
  }
};

export default {
  state,
  mutations,
  actions,
  getters
}