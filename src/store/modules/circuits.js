import axios from '../../axios';

const state = {
  circuits: [],
  activeCircuitId: 0,
  isLoading: false
};

const mutations = {
  LOAD_CIRCUITS(state, themeId) {
    state.isLoading = true;
    axios
      .get(`/circuits?theme=${themeId}`)
      .then(response => response.data)
      .then(result => {
        state.circuits = result;
      })
      .catch(error => {
        // eslint-disable-next-line
        console.log(`Something went wrong while retriving circuits: ${error}`);
      })
      .finally(() => {
        state.isLoading = false;
      });
  },
  LOAD_ALL_CIRCUITS(state) {
    state.isLoading = true;
    axios
      .get(`/circuits`)
      .then(response => response.data)
      .then(result => {
        state.circuits = result;
      })
      .catch(error => {
        // eslint-disable-next-line
        console.log(`Something went wrong while retriving circuits: ${error}`);
      })
      .finally(() => {
        state.isLoading = false;
      });
  },
  SET_ACTIVE_CIRCUIT(state, id) {
    state.activeCircuitId = id;
  }
};

const actions = {
  loadCircuits({ commit, rootState }) {
    commit('LOAD_CIRCUITS', rootState.themes.activeThemeId);
  },
  loadAllCircuits({ commit }) {
    commit('LOAD_ALL_CIRCUITS');
  },
  setActiveCircuit({ commit, dispatch }, id) {
    commit('SET_ACTIVE_CIRCUIT', id);
    dispatch('loadCircuit');
  }
};

const getters = {
  circuits(state, rootState) {
    if (rootState.activeTheme) {
      return state.circuits.filter(x => {
        return x.theme == rootState.activeTheme;
      });
    }
    return state.circuits;
  },
  isCircuitsLoading(state) {
    return state.isLoading;
  },
  activeCircuit(state) {
    return state.activeCircuitId;
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
