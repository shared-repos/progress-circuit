const state = {
  logs: []
};

const mutations = {
  ADD_RECORD(state, content) {
    state.logs.push({
      id: new Date(),
      date: formatTime(),
      content: content
    });
  },
  CLEAR_LOGS(state) {
    state.logs = [];
  }
};

const actions = {
  addLogRecord({ commit }, content) {
    commit('ADD_RECORD', content);
  },
  clearLogs({ commit }) {
    commit('CLEAR_LOGS');
  }
};

const getters = {
  logs(state) {
    return state.logs.sort(function(a, b) {
      return b.id - a.id;
    });
  }
};

function formatTime() {
  let date = new Date();
  let hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
  let minutes =
    date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
  let seconds =
    date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();

  return `${hours}:${minutes}:${seconds}`;
}

export default {
  state,
  mutations,
  actions,
  getters
};
